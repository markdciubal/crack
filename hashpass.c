#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "md5.h"

int main(int argc, char *argv[])
{
    if (argc < 2 || argc > 3)
    {
        printf("Usage: %s input_file output_file\n", argv[0]);
        exit(2);
    }
    else
    {
        // Open first argument as filename for reading.
        FILE *fr = fopen(argv[1], "r");;
        if (!fr)
        {
            printf("Cannot open %s for reading.", argv[1]);
            exit(0);
        }
        else
        {
            fr = fopen(argv[1], "r");
        }
        // Open second argument as filename for writing.
        
        FILE *fw = fopen(argv[2], "w");;
        if (!fw)
        {
            printf("Cannot open %s for writing.", argv[2]);
            exit(0);
        }
        else
        {
            fw = fopen(argv[2], "w");
        }
        char line[100];
        while(fgets(line, 100, fr) != NULL)
        {
            for (int i = 0; i < strlen(line); i++)
            {
                if (line[i] == '\n')
                {
                    line[i] = '\0';
                }
            }
            char *hash = md5(line, strlen(line));
            //printf("%lu\n", strlen(line));
            //printf("%s\n", hash);
            fprintf(fw, "%s\n", hash);
        }
        fclose (fr);
        fclose (fw);
    }
}